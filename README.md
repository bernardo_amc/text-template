# Simple text template engine for rust

This library implements text templates with named placeholder elements like
`Hello ${title }${name}`. The space after title is significant in the sense that
it is only included in the output if title is not empty. This is to prevent
two consecutive whitespaces for example if a person has no title.


Please see https://docs.rs/crate/text-template for API documentation.

## Example
```rust
use text_template::*;
use std::collections::HashMap;
 
let template = Template::from("Hello ${title }${name}");
 
let mut values = HashMap::new();
values.insert("name", "Jane");
 
let text = template.fill_in(&values);

assert_eq!(text.to_string(), "Hello Jane");
assert_eq!(template.to_string(), "Hello ${title }${name}");
```